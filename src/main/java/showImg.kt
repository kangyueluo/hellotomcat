


import boofcv.io.image.UtilImageIO
import java.awt.image.BufferedImage
import java.io.*
import javax.imageio.ImageIO
import javax.servlet.ServletException
import javax.servlet.annotation.WebServlet
import javax.servlet.http.HttpServlet
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse


@WebServlet("/showImg")
class showImg :HttpServlet() {

    /**
     * @see HttpServlet.doGet
     */
    @Throws(ServletException::class, IOException::class)
    override fun doGet(request: HttpServletRequest?, response: HttpServletResponse) {
        try {
            val image: BufferedImage = UtilImageIO.loadImage("/mnt/sdb1/home/koin/IdeaProjects/HelloTomcat/drawable/lena.jpg")

            val data = toByteArray(image, "jpg")

            response.contentType = "image/jpeg"
            val toClient: OutputStream = response.outputStream
            toClient.write(data)
            toClient.close()

        } catch (e: IOException)
        {
            val toClient = response.writer
            response.contentType = "text/html;charset=big5"
            toClient.write("�L�k���}�Ϥ�!")
            toClient.close()
        }
    }

    /**
     * @see HttpServlet.doPost
     */
    @Throws(ServletException::class, IOException::class)
    override fun doPost(request: HttpServletRequest?, response: HttpServletResponse?) {
    }


    // convert BufferedImage to byte[]
    @Throws(IOException::class)
    fun toByteArray(bi: BufferedImage?, format: String?): ByteArray? {
        val baos = ByteArrayOutputStream()
        ImageIO.write(bi, format, baos)
        return baos.toByteArray()
    }

    // convert byte[] to BufferedImage
    @Throws(IOException::class)
    fun toBufferedImage(bytes: ByteArray?): BufferedImage? {
        val `is`: InputStream = ByteArrayInputStream(bytes)
        return ImageIO.read(`is`)
    }


}