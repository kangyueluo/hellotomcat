import java.awt.event.ActionListener
import java.io.IOException
import java.util.*
import javax.servlet.ServletConfig
import javax.servlet.ServletException
import javax.servlet.annotation.WebServlet
import javax.servlet.http.HttpServlet
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse
import javax.swing.Timer


@WebServlet( loadOnStartup = 1,name = "HelloWorld", urlPatterns = ["/"])
class HelloWorld : HttpServlet() {

    private val serialVersionUID = 1L

    override fun init(config: ServletConfig?) {
        super.init(config)

        //Timer(1000, ActionListener { System.out.println(Date()) }).start()
    }

    @Throws(ServletException::class, IOException::class)
    override fun doGet(req: HttpServletRequest, resp: HttpServletResponse) {
        val name = "Koin"
        val writer = resp.writer
        writer.println("<!DOCTYPE html>")
        writer.println("<html>")
        writer.println("<head>")
        writer.println("<title>Hello! Servlet!</title>")
        writer.println("</head>")
        writer.println("<body>")
        writer.println("<h1>Hello! Tomcat! ${name}</h1>")
        writer.println("</body>")
        writer.println("</html>")
    }
}